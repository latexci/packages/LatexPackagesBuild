This project has moved to [git.abstractnonsen.se/latex/latex-packages-build](https://git.abstractnonsen.se/latex/latex-packages-build).

You can find the builds there in the future.
